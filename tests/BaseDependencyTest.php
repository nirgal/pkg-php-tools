<?php
/*
 * Copyright (c) 2014 Mathieu Parent <sathieu@debian.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 * This class tests basic functionnality of \Pkgtools\Base\Dependency.php
 *
 * @copyright Copyright (c) 2014 Mathieu Parent <sathieu@debian.org>
 * @author Mathieu Parent <sathieu@debian.org>
 * @license Expat http://www.jclark.com/xml/copying.txt
 */
class BaseDependencyTest extends PHPUnit\Framework\TestCase {
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Unknown property: 'unknown'
     */
    public function testGetUnknownProperty() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->unknown;
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Unknown property: 'unknown'
     */
    public function testSetUnknownProperty() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->unknown = 'unknown';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Unknown dependency level: 'enhance'
     */
    public function testInvalidLevel() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->level = 'enhance';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Malformed dependency project: 'invalid/project'
     */
    public function testInvalidProject() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->project = 'invalid/project';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid dependency project: pear-extension
     */
    public function testInvalidPearExtension() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->project = 'pear-extension';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Malformed dependency package: 'inv@lid'
     */
    public function testInvalidPackage() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->package = 'inv@lid';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Malformed dependency minVersion: '1~béta'
     */
    public function testInvalidVersion() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->minVersion = '1~béta';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Malformed dependency excludeMinVersion: 'notbool'
     */
    public function testInvalidExcludeVersion() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->excludeMinVersion = 'notbool';
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Malformed dependency original: 'notdep'
     */
    public function testInvalidOriginal() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->original = 'notdep';
    }

    public function testPackageBeginsWithProject() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'project1', 'project1-package1');
        $this->assertEquals('php-project1-package1', $dep->debName());
    }

    public function testMergeProjectPackage() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'project1-something1', 'something1-package1');
        $this->assertEquals('php-project1-something1-package1', $dep->debName());
    }

    public function testMaxAsString() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->maxVersion = '1';
        $this->assertEquals('require:pear-pecl.php.net/json (< 1)', (string) $dep);
    }

    public function testMaxAsDebName() {
        $this->expectOutputString('');
        $dep = new \Pkgtools\Base\Dependency('require', 'pear-pecl.php.net', 'json');
        $dep->maxVersion = '1';
        $this->assertEquals('php-json (<< 1)', $dep->debDependency());
    }
}
